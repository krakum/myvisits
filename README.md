## My Visits
An interactive geographical chart visualizing my travel visits in Europe.

## Technologies
| Name			   | Description        |
| ------------------------ | ------------------ |
| Google Visualization API | GeoCharts          |
| JavaScript               | Scripting language |
| HTML			   | Markup language    |
| CSS			   | Stylesheets        |

![Alt MyVisits](http://i.imgur.com/treoleZ.png)
