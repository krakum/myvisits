google.load('visualization','1',{'packages': ['geochart']});
google.setOnLoadCallback(init);
function init(){
 var data = google.visualization.arrayToDataTable([
    ['Place','Visits'],
    ['Gdansk', 2], ['Gdynia', 2], ['Gotenburg', 2], ['Kalmar', 5], ['Karlshamn', 8], ['Karlskrona', 50], ['Malm�', 3], ['Oslo', 2], ['Copenhagen', 3], ['Ronneby', 35], ['Sopot', 1], ['Stockholm', 2], ['V�xj�', 5]
  ]);
 var options = {width:600, height:400, keepAspectRatio: true, backgroundColor: '#CEE3F6',
                displayMode: 'markers', region: '150', 
                colorAxis: {colors: ['#04B404', '#0080FF']}
               };
 var chart = new google.visualization.GeoChart(document.getElementById('chart'));
 chart.draw(data,options);
}
